import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class Reader implements Observer<News> {

    private String name;

    public Reader(String name) {
        this.name = name;
    }

    public void viewNews() {
        NewsManager.getNews().forEach(news -> news.read(this));
    }


    @Override
    public void onSubscribe(@NonNull Disposable disposable) {

    }

    @Override
    public void onNext(@NonNull News news) {
        System.out.println("Recommended for you " + name);
        System.out.println(news);
        //news.read(this);
    }

    @Override
    public void onError(@NonNull Throwable throwable) {

    }

    @Override
    public void onComplete() {

    }
}
