import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

import java.util.HashSet;
import java.util.Set;

public class News {
    private final String title;
    private final String message;
    private Editor editor;
    private final Set<Reader> readerSet = new HashSet<>();
    private final PublishSubject<News> editorNotifier = PublishSubject.create();
    private final Domain domain;

    private News(String message, String title, Editor editor, Domain domain) {
        this.editor = editor;
        this.message = message;
        editorNotifier.subscribe(editor);
        this.title = title;
        this.domain = domain;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", nr of readers=" + readerSet.size() +
                '}';
    }

    public String read(Reader reader) {
        if (!readerSet.contains(reader)) {
            readerSet.add(reader);
            editorNotifier.onNext(this);
        }

        return toString();
    }

    public int getNrReaders() {
        return readerSet.size();
    }

    public String getTitle() {
        return title;
    }

    public Domain getDomain() {
        return domain;
    }

    public static News getInstance(String message, String title, Editor editor, Domain domain) {
        News news = new News(message, title, editor, domain);
        Observable<News> subject = Subject.just(news);
        domain.getReadersSubscribed().forEach(subject::subscribe);


        return news;
    }
}