import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class NewsManager {

    private static final List<News> newsList = new ArrayList<>();


    public static List<News> getNews() { return newsList; }

    public static Map<Integer, String> getNewsMap() {
        return newsList.parallelStream()
                .collect(Collectors.toMap(News::hashCode, News::toString ));
    }

    public static void addNews(News news) {
        newsList.add(news);
    }

    public static void deleteNews(News news) {
        newsList.remove(news);
    }
}
