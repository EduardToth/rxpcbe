public class Main {

    public static void main(String[] args) {

        //example1();

        example2();

        //example3();
    }

    private static void example3() {
        Domain sport = new Domain(null,"Sport");
        Domain golf = new Domain(sport,"Golf");
        Domain football = new Domain(sport, "Football");

        Editor editor = new Editor("Edy");

        Reader reader = new Reader("Illes");
        Reader reader1 = new Reader("Samy");
        Reader reader2 = new Reader("Toth");


        sport.attachObserver(reader);
        golf.attachObserver(reader1);
        football.attachObserver(reader2);

        editor.addNews("Merge1", "Title1", golf);
    }

    private static void example2() {
//        NewsManager.getNews()
//                .forEach(news -> System.out.println(news + " was viewed by" + news.getNrReaders()
//                        + " readers"));


        Editor editor = new Editor("Edy");

        Reader reader = new Reader("Illes");
        Reader reader1 = new Reader("Eduard");
        Reader reader2 = new Reader("Toth");

        editor.attachObserver(reader);
        editor.attachObserver(reader1);
        editor.attachObserver(reader2);

        editor.addNews("Merge1", "Title1", new Domain(null, "Sport"));
    }

    private static void example1() {
        Editor editor = new Editor("Radu");

        Domain domain = new Domain(null, "Sport");
        editor.addNews("Merge1", "Title1", domain);
        editor.addNews("Merge2", "Title2", domain);
        editor.addNews("Merge3", "Title3", domain);

        editor.deleteNews(NewsManager.getNews().get(1).hashCode());

        editor.updateNews(NewsManager.getNews()
                .get(0)
                .hashCode(), "Yeyyy");

        Reader reader = new Reader("Edy2");
        Reader reader1 = new Reader("David");

        reader.viewNews();
        reader1.viewNews();
    }


}
