import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.PublishSubject;
import org.javatuples.Pair;


public class Editor implements Observer<News> {

    private final String name;
    private final PublishSubject<News> subject = PublishSubject.create();

    public Editor(String name) {
        this.name = name;
    }

    public void addNews(String message, String title, Domain domain) {
        final News news = News.getInstance(message, title, this, domain);

        subject.onNext(news);

        NewsManager.addNews(news);
    }

    public void updateNews(int hashCode, String message) {

        NewsManager.getNews()
                .parallelStream()
                .filter(news -> news.hashCode() == hashCode)
                .findAny()
                .map(news -> Pair.with(news.getTitle(), news.getDomain()))
                .ifPresent(pair -> updateNews(hashCode, message, pair));
    }

    public void updateNews(int hashCode, String message, Pair<String, Domain> newsRelatedData) {
        deleteNews(hashCode);
        addNews(message, newsRelatedData.getValue0(), newsRelatedData.getValue1());
    }

    public void deleteNews(int hashCode) {
        NewsManager.getNews()
                .parallelStream()
                .filter(news -> news.hashCode() == hashCode)
                .findAny()
                .ifPresent(NewsManager::deleteNews);
    }

    @Override
    public void onSubscribe(@NonNull Disposable disposable) {

    }

    @Override
    public void onNext(@NonNull News news) {
        System.out.println(name + " your following article was viewed: "
                + System.lineSeparator()
                + news
                + " was viewed by "
                + news.getNrReaders()
                + " readers");
        System.out.println();
    }

    @Override
    public void onError(@NonNull Throwable throwable) {

    }

    @Override
    public void onComplete() {

    }

    public void attachObserver(Reader reader) {
        subject.subscribe(reader);
    }
}
