
//import io.reactivex.subjects.PublishSubject;
//
//import java.util.stream.Collectors;
//
//public class Domain {
//
//    private final Domain parentDomain;
//    private final String name;
//    private final PublishSubject<News> subject = PublishSubject.create();
//
//
//    public Domain(Domain parentDomain, String name) {
//        this.parentDomain = parentDomain;
//        this.name = name;
//    }
//
//    public Domain getParentDomain() {
//        return parentDomain;
//    }
//
//    public void subscribe(Reader reader) {
//        subject.subscribe(reader);
//    }
//
//    public void notifySubscribers(News news) {
//        Domain domain = this;
//
//        while(domain != null) {
//            domain.subject.onNext(news);
//            domain = domain.getParentDomain();
//        }
//    }
//}

import java.util.HashSet;
import java.util.Set;

public class Domain {

    private final Domain parentDomain;
    private final String name;
    private final Set<Reader> subscribedReaders = new HashSet<>();

    public Domain(Domain parentDomain, String name) {
        this.parentDomain = parentDomain;
        this.name = name;
    }

    public Domain getParentDomain() {
        return parentDomain;
    }

    public void attachObserver(Reader reader) {
        subscribedReaders.add(reader);
    }

    public Set<Reader> getReadersSubscribed() {

        Set<Reader> readers = new HashSet<>();
        Domain domain = this;

        while(domain != null) {
            readers.addAll(domain.subscribedReaders);
            domain = domain.getParentDomain();
        }

        return readers;
    }
}

